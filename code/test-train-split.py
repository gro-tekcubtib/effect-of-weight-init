"""
Author: Alex-Antoine Fortin
Date: Sunday December 16th 2018
Description
Take all images from 101 caltech and splits them into a train/test split directories
"""
import os
import shutil
from random import random
from pathlib import Path

os.makedirs('../inputs/train/', exist_ok=True)
os.makedirs('../inputs/test/', exist_ok=True)

pathlist = Path('../inputs/').glob('**/*.jpg')
for path in pathlist:
    category, filename = str(path).rsplit('/', 2)[1], str(path).rsplit('/', 2)[2]
    os.makedirs(os.path.join('../inputs/train/', category), exist_ok=True)
    os.makedirs(os.path.join('../inputs/test/', category), exist_ok=True)
    if random() < 0.12 : # Go in test set
        shutil.move(str(path), os.path.join('../inputs/test/', category, filename))
    else: # go in train set
        shutil.move(str(path), os.path.join('../inputs/train/', category, filename))
    # print(path_in_str)
