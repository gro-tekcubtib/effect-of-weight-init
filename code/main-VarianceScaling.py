'''
Author: Alex-Antoine Fortin
Date: Thursday, December 6th 2018
Source paper:
http://papers.nips.cc/paper/7338-how-to-start-training-the-effect-of-initialization-and-architecture.pdf
[ ] - Build network (ResNet32)
[ ] - predict CIFAR100, MNIST, MNIST-FASHION (parametrizable)
[ ] - Use different initializer (He Normal, He Uniform, Glorot) (parametrizable)
[ ] - Save accuracy/loss for each epoch
[ ] - Plot accuracy/loss for each epoch
[ ] - Overlay plot
'''
import os
import imp
import json
import tensorflow as tf
t = imp.load_source('tools', '../code/tools.py')

OUTPUT_FILENAME = 'he_normal_non-truncated-101Caltech-lr0.1.json'

with tf.device('/device:GPU:0'):
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    session = tf.Session(config=config)

# LOAD DATASET
INPUT_DIM = (256, 256, 3) # > (197, 197, 3) for ResNet50
BATCH_SIZE = 44
NUM_CLASS = 102 # Number of output neurons

#==============================
#Generating and cropping images
#==============================
genTrain = t.genTrain()
genTest = t.genTest()
traingen = t.traingen(genTrain, BATCH_SIZE)
testgen = t.testgen(genTest, BATCH_SIZE)

crop_traingen = t.crop_gen(traingen, INPUT_DIM)
crop_testgen = t.crop_gen(testgen, INPUT_DIM)

# BUILD NETWORK
from keras.applications.resnet50 import ResNet50
from keras.models import Model
from keras.layers import Dense, Dropout, Activation, Flatten, Input
from keras.layers import GlobalAveragePooling2D, BatchNormalization
from keras import regularizers
from keras import backend as K
from keras.callbacks import TensorBoard, ModelCheckpoint, LearningRateScheduler
from keras.optimizers import Adam

INIT_LR = 0.05
NB_EPOCH = 200
NB_STEPS_PER_EPOCH = int((8928/BATCH_SIZE)/2) # 1 epoch is 1/2 of the data
NB_VAL_STEPS_PER_EPOCH = int(216/BATCH_SIZE)

##################
from VarianceScaling import VarianceScaling
weightsInitializer = VarianceScaling(scale=2.0, mode='fan_in', distribution='normal')
##################

with tf.device('/device:GPU:0'):
    input_layer = Input(shape=INPUT_DIM,
                        name='InputLayer')
    model = ResNet50(
        include_top=True,
        weights=None, # None (random init) or 'imagenet'
        input_tensor=input_layer,
        input_shape=INPUT_DIM,
        pooling=None,
        classes=NUM_CLASS # optional number of classes to classify images into, only to be specified if include_top is True, and if no weights argument is specified.
        )
    # Setting initial weights according desired distribution
    initial_weights = model.get_weights()
    new_weights = [weightsInitializer(w.shape).eval(session=session) for w in initial_weights]
    model.set_weights(new_weights)
    # Optimizer
    optimizer = Adam(lr=INIT_LR, decay=0.003)
    # Compile
    model.compile(optimizer=optimizer, loss='categorical_crossentropy',
                  metrics=['accuracy'])
    # Fit
    hist = model.fit_generator(
            generator=crop_traingen,
            steps_per_epoch=NB_STEPS_PER_EPOCH,
            epochs=NB_EPOCH,
            workers=14,
            validation_data=crop_testgen,
            validation_steps=NB_VAL_STEPS_PER_EPOCH)

with open(os.path.join('../outputs/', OUTPUT_FILENAME), 'w') as f:
    json.dump(hist.history, f)
